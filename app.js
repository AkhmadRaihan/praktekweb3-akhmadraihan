const exp=require("express")
const appServer=exp()
const port=3000;

const parser= require("body-parser")

appServer.use(parser.json())

const jsonBody=parser.json()

appServer.get("/", function(req,res){
    res.send("IDE LPKIA");
});

appServer.get("/mahasiswa3si",function(req,res){
    var mhs=["rosa","syafna","akbar","agil","akhmad","deni","fidly","ilyas","panji","rahmat","ridwan","rivaldi"];
    res.send(mhs);
});

appServer.post("/tambahmahasiswa3si",jsonBody,function(req,res){
    res.send('Selamat Datang Ditambah mahasiswa dengan method post');
});

appServer.put('/methodput', function (req, res) {
    res.send('Ini merupakan Halaman method put');
  });

appServer.delete('/methoddelete', function (req, res) {
    res.send('Ini merupakan Halaman method delete')
  })

appServer.listen(port,function(req,res){
    console.log("LPKIA "+port);
});
